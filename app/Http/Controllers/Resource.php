<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductFormRequest;
use App\Dish;
use App\Type;
use App\User;

class Resource extends Controller
{
    //Gestion dish y rutas
    public function index()
    {
        $dishes = Dish::paginate(4);
        $type = Type::all();
        $user = User::all();
       //$families = Family::paginate(4);
        return view('dish.index', ['dishes' => $dishes, 'types' => $type, 'users' => $user]);

    }
    public function create()
    {
        
        return view('dish.create');
    }
    public function store(Request $request)
    {
        
        $this->validate($request, [
        
        'name' => 'required|max:40',
        'descripcion' => 'required|max:100',
        'user_id' => 'required|max:4',
        'type_id' => 'required|max:4',
        ]);

        $dish = new Dish($request->all());
        $dish->save();
        
        return redirect('/dish');
    }
    public function show($id, Request $request)
    {
//        
        $dish = Resource::findOrFail($id);
            return view('dish.show', ['dish' => $dish]);
        
    }
public function destroy($id, Request $request)
    {
        Resource::find($id)->delete();
        
        
            return redirect('/families');
    }
}
