@extends('layouts.app')
@section('content')

    <h1>Crear Plato</h1>
    <div class="form">
    <form  action="/dish" method="post">
    {{ csrf_field() }}


    <div class="form-group">
        <label>Nombre: </label>
        <input type="text" name="name" >
        {{ $errors->first('name') }}
    </div>
    <div class="form-group">
        <label>Descripcion: </label>
        <input type="text" name="description" >
        {{ $errors->first('description') }}
    </div>
    <div class="form-group">
        <label>Id Usuario: </label>
        <input type="text" name="user_id" >
        {{ $errors->first('user_id') }}
    </div>
    <div class="form-group">
        <label>Id Tipo: </label>
        <input type="text" name="type_id">
        {{ $errors->first('type_id') }}
    </div>
    <input type="submit" value="Guardar">
    </form>
    </div>

@endsection('content')
