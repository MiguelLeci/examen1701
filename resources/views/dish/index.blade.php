@extends('layouts.app')

@section('title', 'Platos')



@section('content')
    <a class="navbar-brand" href="{{ url('/dish/create') }}">
                        {{ config('AÑADIR', 'AÑADIR') }}
            </a>
    @foreach ($dishes as $dish)
            
            <tr>
                
                <td>  {{ $dish->id }} </td><br>
                <td> <b> {{ $dish->name }} </b> </td><br>
                <td>  {{ $dish->description }} </td><br>
                
                @foreach ($users as $user)
                    @if ($user->id == $dish->user_id)
                        <td>  {{ $user->name }} </td><br>
                        @continue
                    @endif
                    
                @endforeach
                
                @foreach ($types as $type)
                    @if ($type->id == $dish->type_id)
                        <td>  {{ $type->name }} </td><br>
                        @continue
                    @endif
                @endforeach
                    <form method="post" action="/dish/{{ $dish->id }}">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}

                        @can('delete', $dish)
                        <input type="submit" value="Borrar">
                        @endcan

                        @can('view', $dish)
                        <a href="/dish/{{ $dish->id }}"> Ver </a>
                        @endcan
                    </form>
               
                
            </tr>
            <br>

        @endforeach
    {{ $dishes->render() }}

@endsection